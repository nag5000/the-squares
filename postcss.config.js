module.exports = {
  parser: 'postcss-scss',
  plugins: {
    'postcss-import': {},
    'postcss-preset-env': {
      stage: 2,
      features: {
        'nesting-rules': true
      }
    },
    'autoprefixer': {},
    'cssnano': {}
  }
}
