const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');

const PROD = process.env.NODE_ENV === 'production';
const NPM_TARGET = process.env.npm_lifecycle_event;

module.exports = {
  entry: {
    index: './src/scripts/index.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
  },
  plugins: [
    NPM_TARGET !== 'start' ? new CleanWebpackPlugin(['dist']) : null,
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    }),
    new ScriptExtHtmlWebpackPlugin({
      sync: ['index']
    })
  ].filter(Boolean),
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    }, {
      test: /\.html$/,
      use: [{
        loader: 'html-loader',
        options: {
          attrs: ['img:src', 'use:href', 'link:href'],
          minimize: PROD,
          removeComments: true,
          collapseWhitespace: false
        }
      }]
    }, {
      test: /\.css$/,
      use: [
        PROD ? MiniCssExtractPlugin.loader : 'style-loader',
        { loader: 'css-loader', options: { importLoaders: 1 } },
        'postcss-loader'
      ]
    }, {
      test: /\.(png|jpg|svg|ico)$/,
      use: [{
        loader: 'file-loader',
        options: {
          outputPath: 'assets/'
        }
      }]
    }]
  }
};
