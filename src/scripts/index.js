import '../styles/index.css';

import Settings from './settings.js';
import Game from './game.js';

import dialogPolyfill from 'dialog-polyfill';
let dialogs = document.querySelectorAll('dialog');
dialogs.forEach(dialogPolyfill.registerDialog);

startGamePreview();

const startForm = document.getElementById('startForm');
startForm.addEventListener('submit', function(e) {
  e.preventDefault();
  startNewGame();
});

startForm.querySelectorAll('input[type=range]').forEach(el => {
  el.addEventListener('input', rangeInputOnChange);
  rangeInputOnChange.call(el);
});

const initialSpeedInput = document.getElementById('initialSpeedInput');
initialSpeedInput.min = 0; // for initialSpeed == 'auto';
initialSpeedInput.max = Settings.speedCount;
initialSpeedInput.addEventListener('input', function(e) {
  const initialSpeedOutput = this.previousElementSibling;
  initialSpeedOutput.innerText = (this.value == 0 ? 'auto' : this.value);
  e.stopImmediatePropagation();
});
initialSpeedInput.dispatchEvent(new Event('input'));

const gameOverMenuBtn = document.getElementById('gameOverMenuBtn');
gameOverMenuBtn.addEventListener('click', function(e) {
  const gameOverDialog = document.getElementById('gameOverDialog');
  gameOverDialog.close();

  const startDialog = document.getElementById('startDialog');
  startDialog.show();

  startGamePreview();
});

function startNewGame() {
  const settings = new Settings();
  settings.loadFromGUI();

  const startDialog = document.getElementById('startDialog');
  startDialog.close();

  _startGame(settings);
}

function startGamePreview() {
  const settings = new Settings({
    previewMode: true,
    squareCount: 3,
    squareSize: 120,
    squareMinSize: undefined,
    addSquareOnClick: false,
    initialSpeed: 1,
    changeSpeedOnCollisions: false,
    splitSquaresOnCollisions: false,
    fieldSize: {
      width: window.innerWidth,
      height: window.innerHeight
    }
  });

  _startGame(settings);
}

function _startGame(settings) {
  if (window.game) {
    window.game.destroy();
  }

  let game = new Game(settings);
  window.game = game;
  game.start();
}

function rangeInputOnChange() {
  let output = this.previousElementSibling;
  if (output && output.tagName === 'OUTPUT') {
    // use <output> to display current value.
    output.innerText = this.value;
  }
}
