import Square from './square.js';
import Field from './field.js';
import Collisions from './collisions.js';

export default class Game {
  constructor(settings, field = null) {
    if (!field) {
      let squares = this.generateSquares(settings);
      field = new Field(settings.fieldSize, squares);
    }

    this.field = field;
    this.settings = settings;
    this.collisions = new Collisions(settings);

    this._statistics = {
      collisionsCount: {
        label: 'Всего столкновений',
        value: 0
      },
      maxSquaresCount: {
        label: 'Максимальное кол-во квадратов на поле',
        value: 0
      }
    };

    this._stopped = false;
    this._animate = this._animate.bind(this);

    let fieldContainer = document.getElementById('fieldContainer');
    fieldContainer.appendChild(field.element);

    this.onFieldClick = this.onFieldClick.bind(this);
    this.field.element.addEventListener('click', this.onFieldClick);

    this.onWindowResize = this.onWindowResize.bind(this);
    window.addEventListener('resize', this.onWindowResize);

    this.onKeyUp = this.onKeyUp.bind(this);
    document.addEventListener('keyup', this.onKeyUp);
  }

  destroy() {
    this._stop();
    window.removeEventListener('resize', this.onWindowResize);
    document.removeEventListener('keyup', this.onKeyUp);
    this.field.destroy();
  }

  start() {
    this._animate();
  }

  gameover() {
    this._stop();

    let title;

    let squaresCount = this.field.squares.length;
    if (squaresCount === 1) {
      this.field.squares[0].element.classList.add('square--winner');
      title = 'Победитель!';
    } else if (squaresCount === 0) {
      title = 'Ничья!';
    } else {
      title = 'Игра окончена';
    }

    let titleEl = document.getElementById('gameOverTitle');
    titleEl.innerText = title;

    let statFragment = document.createDocumentFragment();
    for (let key in this._statistics) {
      let stat = this._statistics[key];
      let dt = document.createElement('dt');
      let dd = document.createElement('dd');
      dt.innerText = stat.label;
      dd.innerText = stat.value;
      statFragment.appendChild(dt);
      statFragment.appendChild(dd);
    }

    let statEl = document.getElementById('gameOverStatistics');
    while (statEl.firstChild) {
      statEl.removeChild(statEl.firstChild);
    }

    statEl.appendChild(statFragment);

    const gameOverDialog = document.getElementById('gameOverDialog');
    gameOverDialog.show();
  }

  onWindowResize(e) {
    this.field.updateSize({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

  onKeyUp(e) {
    if (!this.settings.previewMode && e.key === 'Escape') {
      this.gameover();
    }
  }

  onFieldClick(e) {
    if (this._stopped || !this.settings.addSquareOnClick) {
      return;
    }

    let id = 's' + parseInt(e.timeStamp);
    let size = this.settings.squareSize;

    let x = Math.ceil(e.offsetX - size / 2);
    let y = Math.ceil(e.offsetY - size / 2);
    let minX = 0;
    let minY = 0;
    let maxX = this.field.size.width - size;
    let maxY = this.field.size.height - size;
    let pos = {
      x: Math.min(Math.max(x, minX), maxX),
      y: Math.min(Math.max(y, minY), maxY)
    };

    let velocity = this.getRandomVelocity(this.settings);
    let square = new Square(id, pos, size, velocity, 'square--new');

    if (this.field.squares.some(sq => sq.intersectsWith(square))) {
      return;
    }

    this.field.addSquares([square]);
  }

  render() {
    let squares = this.field.squares;
    let squaresCount = squares.length;
    let collisions = this.collisions;
    let stat = this._statistics;

    collisions.oldSquares = [];
    collisions.newSquares = [];

    for (let i = 0; i < squaresCount; i++) {
      let square = squares[i];

      collisions.handleBoundaryIntersections(square, this.field.size);

      for (let k = i + 1; k < squaresCount; k++) {
        let otherSquare = squares[k];
        let hasCollision = collisions.handleCollision(square, otherSquare);
        if (hasCollision) {
          stat.collisionsCount.value++;
        }
      }

      square.moveToNextPosition();
    }

    this.field.removeSquares(collisions.oldSquares);
    this.field.addSquares(collisions.newSquares);

    stat.maxSquaresCount.value = Math.max(
      stat.maxSquaresCount.value,
      squaresCount
    );
  }

  generateSquares(settings) {
    let squares = [];
    let {
      squareCount,
      squareSize,
      fieldSize
    } = settings;

    for (let i = 0; i < squareCount; i++) {
      let id = `s${i}`;
      let pos = {
        x: this.getRandomInt(0, fieldSize.width - squareSize),
        y: this.getRandomInt(0, fieldSize.height - squareSize)
      };

      let velocity = this.getRandomVelocity(settings);
      let square = new Square(id, pos, squareSize, velocity);

      let attempts = 0;
      while (squares.some(s => s.intersectsWith(square))) {
        square.pos = {
          x: this.getRandomInt(0, fieldSize.width - squareSize),
          y: this.getRandomInt(0, fieldSize.height - squareSize)
        };

        if (++attempts > 100) {
          return squares;
        }
      }

      squares.push(square);
    }

    return squares;
  }

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  // NOTE: Учтено ограничение на движение под углом, кратным 45 градусам.
  getRandomVelocity(settings) {
    let speed = settings.initialSpeed;
    let vx, vy;

    if (speed === 'auto') {
      const values = settings._velocityValues;
      let valueIndex = this.getRandomInt(0, values.length - 1);
      vx = values[valueIndex];
      vy = this.getRandomInt(-1, 1) * vx;
    } else if (speed > 0) {
      vx = this.getRandomInt(-1, 1) * speed;
      vy = this.getRandomInt(-1, 1) * speed;
    } else {
      throw new Error('Invalid speed value: ' + speed);
    }

    if (vx === 0 && vy === 0) {
      return this.getRandomVelocity(settings);
    } else {
      return {x: vx, y: vy};
    }
  }

  _stop() {
    this._stopped = true;
  }

  _animate() {
    if (this._stopped) {
      return;
    }

    this.render();

    if (!this.settings.previewMode && this.field.squares.length <= 1) {
      this.gameover();
      return;
    }

    requestAnimationFrame(this._animate);
  }
}
