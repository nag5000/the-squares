module.exports = {
  globals: {},
  root: true,
  parserOptions: {
    ecmaVersion: 2017,
    sourceType: 'module'
  },
  extends: [
    'eslint:recommended'
  ],
  env: {
    browser: true,
    es6: true
  },
  rules: {
    'no-unused-vars': ['error', {
      args: 'none'
    }],
    'no-console': 'off',
    'no-constant-condition': ['error', {
      checkLoops: false
    }]
  },
  overrides: [{
    files: [
      'webpack.*.js',
      'postcss.config.js'
    ],
    env: {
      browser: false,
      es6: true,
      commonjs: true,
      node: true
    }
  }]
};
