export default class Settings {
  constructor(options) {
    let hash = Object.assign({
      previewMode: false,
      squareCount: 33,
      squareSize: 80,
      squareMinSize: 20,
      addSquareOnClick: true,
      initialSpeed: 'auto',
      changeSpeedOnCollisions: true,
      splitSquaresOnCollisions: true,
      fieldSize: {
        width: window.innerWidth,
        height: window.innerHeight
      }
    }, options);

    for (let key in hash) {
      this[key] = hash[key];
    }

    this._velocityValues = this.getVelocityValues();
  }

  loadFromGUI() {
    let elem = (id) => document.getElementById(id);

    this.squareCount = parseInt(elem('squareCountInput').value);
    this.squareSize = parseInt(elem('squareSizeInput').value);
    this.squareMinSize = parseInt(elem('squareMinSizeInput').value);
    this.changeSpeedOnCollisions = elem('changeSpeedOnCollisionsCheckbox').checked;

    this.fieldSize = {
      width: window.innerWidth,
      height: window.innerHeight
    };

    let guiSpeed = parseInt(elem('initialSpeedInput').value);
    this.initialSpeed = (guiSpeed === 0)
      ? 'auto'
      : Settings.getSpeedList()[guiSpeed - 1];
  }

  // Return possible velocity values according to the possible speed values:
  // e.g., [1, 2] => [-2, -1, 0, 1, 2]
  getVelocityValues() {
    let speedList = Settings.getSpeedList();
    return speedList.reverse().map(x => -x).concat(0, speedList);
  }

  static get speedStep() {
    return 0.5;
  }

  static get minSpeed() {
    return 0.5;
  }

  static get speedCount() {
    return 4;
  }

  // Allowed speed values: [0.5, 1, 1.5, 2].
  static getSpeedList() {
    return Array.from(
      { length: this.speedCount },
      (v, k) => this.minSpeed + k * this.speedStep
    );
  }
}
