import Settings from './settings.js';
import Square from './square.js';

const { speedStep, minSpeed } = Settings;

export const COLLISION_SIDE = {
  top: 0,
  left: 1,
  right: 2,
  bottom: 3,

  reverse(x) {
    switch(x) {
      case 0: return 3;
      case 1: return 2;
      case 2: return 1;
      case 3: return 0;
      default: throw new Error('Invalid value of COLLISION_SIDE: ' + x);
    }
  }
};

export default class Collisions {
  constructor(settings) {
    this.settings = settings;

    this.oldSquares = [];
    this.newSquares = [];
    this._avoidCollisions = {};
  }

  handleBoundaryIntersections(square, boxSize) {
    let {x: vx, y: vy} = square.velocity;

    if (square.left <= 0) {
      square.velocity.x = vx > 0 ? vx : -vx;
    } else if (square.right >= boxSize.width) {
      square.velocity.x = vx < 0 ? vx : -vx;
    }

    if (square.top <= 0) {
      square.velocity.y = vy > 0 ? vy : -vy;
    } else if (square.bottom >= boxSize.height) {
      square.velocity.y = vy < 0 ? vy : -vy;
    }
  }

  handleCollision(square1, square2) {
    let intersect = square1.intersectsWith(square2);
    if (intersect) {
      if (!this.shouldAvoidCollision(square1, square2)) {
        let collisionInfo = this.getCollisionInfo(square1, square2);

        this.avoidNextCollision(square1, square2);

        this.handleVelocity(square1, square2, collisionInfo);

        if (this.settings.changeSpeedOnCollisions) {
          this.handleSpeed(square1, square2, collisionInfo);
        }

        if (this.settings.splitSquaresOnCollisions) {
          this.oldSquares.push(square1, square2);

          let splittedSquares = this.splitSquares(
            square1,
            square2,
            collisionInfo
          );
          this.newSquares.push(...splittedSquares);
        }

        return true;
      }
    } else {
      this.allowNextCollision(square1, square2);
    }

    return false;
  }

  getCollisionInfo(square1, square2) {
    let distX = square1.centerX - square2.centerX;
    let distY = square1.centerY - square2.centerY;
    let side;

    if (Math.abs(distX) > Math.abs(distY)) {
      // Collision of vertical sides.
      side = distX < 0 ? COLLISION_SIDE.right : COLLISION_SIDE.left;
    } else if (Math.abs(distX) < Math.abs(distY)) {
      // Collision of horizontal sides.
      side = distY < 0 ? COLLISION_SIDE.bottom : COLLISION_SIDE.top;
    } else {
      // Collision of corners.
      // For simplicity use previous behaviour.
      side = distY < 0 ? COLLISION_SIDE.bottom : COLLISION_SIDE.top;
    }

    return { side, distX, distY };
  }

  handleVelocity(square1, square2, collisionInfo) {
    let vx1 = square1.velocity.x;
    let vy1 = square1.velocity.y;
    let vx2 = square2.velocity.x;
    let vy2 = square2.velocity.y;

    switch(collisionInfo.side) {
      case COLLISION_SIDE.top:
        square1.velocity.y = Math.abs(vy1 || vy2);
        square2.velocity.y = -Math.abs(vy2 || vy1);
        break;

      case COLLISION_SIDE.right:
        square1.velocity.x = -Math.abs(vx1 || vx2);
        square2.velocity.x = Math.abs(vx2 || vx1);
        break;

      case COLLISION_SIDE.bottom:
        square1.velocity.y = -Math.abs(vy1 || vy2);
        square2.velocity.y = Math.abs(vy2 || vy1);
        break;

      case COLLISION_SIDE.left:
        square1.velocity.x = Math.abs(vx1 || vx2);
        square2.velocity.x = -Math.abs(vx2 || vx1);
        break;
    }
  }

  /**
   * Простая логика изменения скорости в зависимости от размеров
   * столкнувшихся квадратов. Работает, если выставлена соответствующая
   * опция в игре.
   * Для интереса сделано, что меньший квадрат при столкновении ускоряется.
   */
  handleSpeed(square1, square2, collisionInfo) {
    let sv1 = square1.velocity;
    let sv2 = square2.velocity;

    let sv1Inc;
    let sv2Inc;

    if (square1.size > square2.size) {
      sv1Inc = -1;
      sv2Inc = 2;
    } else if (square1.size < square2.size) {
      sv1Inc = 2;
      sv2Inc = -1;
    } else {
      sv1Inc = -1;
      sv2Inc = -1;
    }

    switch(collisionInfo.side) {
      case COLLISION_SIDE.top:
      case COLLISION_SIDE.bottom:
        square1.velocity.y = Math.sign(sv1.y) * Math.max(
          Math.abs(sv1.y) + speedStep * sv1Inc,
          minSpeed
        );
        square2.velocity.y = Math.sign(sv2.y) * Math.max(
          Math.abs(sv2.y) + speedStep * sv2Inc,
          minSpeed
        );
        break;

      case COLLISION_SIDE.left:
      case COLLISION_SIDE.right:
        square1.velocity.x = Math.sign(sv1.x) * Math.max(
          Math.abs(sv1.x) + speedStep * sv1Inc,
          minSpeed
        );
        square2.velocity.x = Math.sign(sv2.x) * Math.max(
          Math.abs(sv2.x) + speedStep * sv2Inc,
          minSpeed
        );
        break;
    }
  }

  splitSquares(square1, square2, collisionInfo) {
    let { squareMinSize } = this.settings;
    let collisionSide = collisionInfo.side;

    let splitted = [];

    if (square1.size > squareMinSize) {
      let children = this.splitSquare(square1, collisionSide);
      splitted.push(...children);
    }

    if (square2.size > squareMinSize) {
      let children = this.splitSquare(
        square2,
        COLLISION_SIDE.reverse(collisionSide)
      );
      splitted.push(...children);
    }

    return splitted;
  }

  splitSquare(original, collisionSide) {
    let initialPositions;
    let velocities;

    let {x: vx, y: vy} = original.velocity;
    let avx = Math.abs(vx || vy);
    let avy = Math.abs(vy || vx);

    switch(collisionSide) {
      case COLLISION_SIDE.top:
        initialPositions = [
          { x: original.left, y: original.top },
          { x: original.centerX, y: original.top }
        ];
        velocities = [
          { x: -avx, y: avy },
          { x: avx, y: avy }
        ];
        break;

      case COLLISION_SIDE.right:
        initialPositions = [
          { x: original.centerX, y: original.top },
          { x: original.centerX, y: original.centerY }
        ];
        velocities = [
          { x: -avx, y: -avy },
          { x: -avx, y: avy }
        ];
        break;

      case COLLISION_SIDE.bottom:
        initialPositions = [
          { x: original.left, y: original.centerY },
          { x: original.centerX, y: original.centerY }
        ];
        velocities = [
          { x: -avx, y: -avy },
          { x: avx, y: -avy }
        ];
        break;

      case COLLISION_SIDE.left:
        initialPositions = [
          { x: original.left, y: original.top },
          { x: original.left, y: original.centerY }
        ];
        velocities = [
          { x: avx, y: -avy },
          { x: avx, y: avy }
        ];
        break;

      default:
        throw new Error('Invalid value of collisionSide: ' + collisionSide);
    }

    let newSize = Math.floor(original.size / 2);
    return Array.from({length: 2}, (_v, i) => i).map(i => {
      let sq = new Square(
        `${original.id}_${i}`,
        initialPositions[i],
        newSize,
        velocities[i],
        'square--new'
      );

      sq.moveToNextPosition();
      return sq;
    });
  }

  shouldAvoidCollision(square1, square2) {
    let dict = this._avoidCollisions;
    return (square1.id in dict && square2.id in dict[square1.id]);
  }

  avoidNextCollision(square1, square2) {
    let dict = this._avoidCollisions;
    dict[square1.id] = dict[square1.id] || {};
    dict[square1.id][square2.id] = true;
  }

  allowNextCollision(square1, square2) {
    let dict = this._avoidCollisions;
    if (square1.id in dict) {
      delete dict[square1.id][square2.id];
    }
  }
}
