export default class Square {
  constructor(id, pos, size, velocity, cssClass = '') {
    this.id = id;
    this.size = size;
    this.velocity = velocity;

    let el = document.createElement('div');
    el.id = id;
    el.className = `square ${cssClass}`;
    el.style.width = size + 'px';
    el.style.height = size + 'px';
    this.element = el;

    let elBody = document.createElement('div');
    elBody.className = 'square__content';
    el.appendChild(elBody);

    this.pos = pos;
  }

  destroy() {
    // Play css animation, then remove element.
    this.element.classList.add('square--hide');
    setTimeout(() => {
      this.element.remove();
    }, 200);
  }

  get pos() {
    return this._pos;
  }

  set pos(value) {
    this._pos = value;
    this.element.style.transform = `translate(${this.left}px, ${this.top}px)`;
  }

  get left() {
    return this._pos.x;
  }

  get top() {
    return this._pos.y;
  }

  get right() {
    return this._pos.x + this.size;
  }

  get bottom() {
    return this._pos.y + this.size;
  }

  get centerX() {
    return this.left + this.size / 2;
  }

  get centerY() {
    return this.top + this.size / 2;
  }

  intersectsWith(square) {
    return Square.intersect(this, square);
  }

  moveToNextPosition() {
    this.pos = {
      x: this._pos.x + this.velocity.x,
      y: this._pos.y + this.velocity.y
    };
  }

  static intersect(a, b) {
    return (a.left <= b.right &&
            b.left <= a.right &&
            a.top <= b.bottom &&
            b.top <= a.bottom);
  }
}
