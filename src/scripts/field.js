export default class Field {
  constructor(size, squares) {
    this.size = size;
    this.squares = squares;

    let el = document.createElement('div');
    el.className = 'field';
    this.element = el;

    this.updateSize(size);
    this._appendSquaresToDOM(squares);
  }

  destroy() {
    this.element.remove();
  }

  updateSize(size) {
    this.size = size;
    this.element.style.width = size.width + 'px';
    this.element.style.height = size.height + 'px';
  }

  removeSquares(squares) {
    squares.forEach(sq => {
      sq.destroy();
      let index = this.squares.indexOf(sq);
      if (index !== -1) {
        this.squares.splice(index, 1);
      }
    });
  }

  addSquares(squares) {
    this.squares.push(...squares);
    this._appendSquaresToDOM(squares);
  }

  _appendSquaresToDOM(squares) {
    let squaresFragment = document.createDocumentFragment();
    squares.forEach(square => {
      squaresFragment.appendChild(square.element);
    });
    this.element.appendChild(squaresFragment);
  }
}
