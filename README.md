Тестовое задание "Квадраты"
=============
По сути "простая" игра, с отталкивающимися квадратами друг от друга и от стен. При столкновениях квадраты распадаются и при достижении минимального размера исчезают. Победитель - последний квадрат на поле.

Полные требования смотри в файле [ЗАДАНИЕ.pdf](./ЗАДАНИЕ.pdf).

Запуск
=============
Visit [https://nag5000.gitlab.io/the-squares/](https://nag5000.gitlab.io/the-squares/) or `npm i && npm start` or open `./dist/index.html`

- `npm start` to launch app on localhost.
- `npm run build` to build into `/dist`.

Заметки к тестовому заданию
=============
- В `.browserslistrc` заявлены только свежие браузеры (использовал ES2015+, css custom variables, html5 dialog). За неимением Сафари в нем не тестировал.
- Дополнительные условия тоже сделал, включаются в опциях в меню игры.
